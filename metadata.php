<?php
/*    Please retain this copyright header in all versions of the software
 *
 *    Copyright (C) Josef A. Puckl | eComStyle.de
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see {http://www.gnu.org/licenses/}.
 */

$sMetadataVersion   = '2.0';
$aModule            = [
    'id'            => 'ecs_showmodulepassword',
    'title'         => '<strong style="color:#04B431;">e</strong><strong>ComStyle.de</strong>:  <i>ShowModulePassword</i>',
    'description'   => '<i>Passwort in den Moduleinstellungen im Klartext</i>',
    'thumbnail'     => 'ecs.png',
    'version'       => '1.0.1',
    'author'        => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'email'         => 'info@ecomstyle.de',
    'url'           => 'https://ecomstyle.de',
    'extend' => [
    ],
    'blocks' => [
        ['template' => 'module_config.tpl', 'block' => 'admin_module_config_var_type_password', 'file' => '/views/blocks/admin_module_config_var_type_password.tpl'],
    ],
];
